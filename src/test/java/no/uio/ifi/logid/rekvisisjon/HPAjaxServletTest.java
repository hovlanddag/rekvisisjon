package no.uio.ifi.logid.rekvisisjon;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;

public class HPAjaxServletTest {

	public static String update_endpoint = "http://localhost:9090/fuseki/rekvisisjon-store/update";
	public static String query_endpoint = "http://localhost:9090/fuseki/rekvisisjon-reason/query";
	
	
	@Test
	public void testDoGetHttpServletRequestHttpServletResponse() throws ServletException, IOException {
		MockServletContext ctxt = new MockServletContext();
		ctxt.addInitParameter("configfolder", "src/test/resources");
		ctxt.addInitParameter("queryendpoint", query_endpoint);
		ctxt.addInitParameter("updateendpoint", update_endpoint);
		
		MockHttpServletRequest request = new MockHttpServletRequest(ctxt);
		
		MockHttpServletResponse response = new MockHttpServletResponse();
		HPAjaxServlet ctrl = new HPAjaxServlet();
		
		request.addParameter("operation","getSubclasses");
		request.addParameter("class","HP_0008872");
		new HPAjaxServlet().doGet(request, response);
		System.out.println(response.getContentLength());
	}
		

}
