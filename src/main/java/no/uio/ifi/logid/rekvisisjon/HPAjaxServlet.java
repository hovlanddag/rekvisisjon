	package no.uio.ifi.logid.rekvisisjon;
	
	import java.io.BufferedWriter;
	import java.io.IOException;
	import java.io.PrintWriter;
	import java.util.ArrayList;
	import java.util.List;
	import java.util.Set;
	
	import javax.servlet.RequestDispatcher;
	import javax.servlet.ServletContext;
	import javax.servlet.ServletException;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;
	import javax.servlet.http.HttpSession;
	
	import org.apache.jena.reasoner.ValidityReport;
	import org.json.simple.JSONArray;
	import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
	
	import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory;
	import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntology;
	import no.uio.ifi.logid.rekvisisjon.hpo.Diagnosis;
	import no.uio.ifi.logid.rekvisisjon.hpo.DiagnosisBean;
	import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation;
	import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotationsFileReader;
	import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotationsList;
	import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.HPUtils;
import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory;
	import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntologyFile;
	import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntologySequoia;
	import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeState;
	import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;
	
	public class HPAjaxServlet extends HttpServlet {
	
		/**
		 * 
		 */
		private static final long serialVersionUID = 8710481308139430462L;
		private static final Logger LOG = LoggerFactory.getLogger(HPAjaxServlet.class);
	
	
	
	
		public HPAjaxServlet() {
			// TODO Auto-generated constructor stub
		}
	
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			this.doGetOrPost(request, response);
		}
	
		private void doGetOrPost(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException {
			try {
				RequisitionController.setupDataStructures(request.getServletContext());
			} catch (InterruptedException e1) {
				e1.printStackTrace();
				throw new ServletException("Setup was interrupted: " + e1.toString());
			}
	
			String op = (String) request.getParameter("operation");
			HttpSession ses = request.getSession();
			PhenotypeState ps = (PhenotypeState) ses.getAttribute("phenotype");
			if(ps == null){
				ps = new PhenotypeState();
				ses.setAttribute("phenotype", ps);
			}
			if(op == null){
				LOG.error("Request to HPOnto with no operation (" + op + ")");
				response.sendError(400, "Request to HPOnto without operation get parameter");
			} else if(op.equals("getSubclasses")){
				PhenotypeOntology hp = OntologyFactory.getOntology();
				String hpcls = (String) request.getParameter("class");
				if(hpcls == null){
					LOG.error("Request to HPOnto with no HP class");
					response.sendError(400, "Request to HPOnto with no class");
				}	
				try {
					String hpoparent = (String) request.getParameter("parent");
					List<Tickbox> subcls = hp.getSubHPOEntries(hpoparent + "/" + hpcls, hpcls, true);
					if(subcls.isEmpty()){
						PrintWriter wr = response.getWriter();
						wr.println("<p></p>");
						return;
					}
					request.setAttribute("hplist", subcls);
					RequestDispatcher rd = request.getRequestDispatcher("/subclasses.jsp");
					rd.forward(request, response);
				} catch (HPOException e) {
					LOG.error("Problem getting subclasses of: " + hpcls + ". Exception: " + e.toString());
					e.printStackTrace();
					request.setAttribute("error", "Could not get subclasses of " + hpcls);
				}
			} else if (op.equals("setAnnotation")){
				LOG.debug("Setting annotation...");
				try {
					String value = request.getParameter("value");
					String hpo_code = request.getParameter("class");
					if(!setPhenotype(ps, hpo_code, value)){
						request.setAttribute("message", "inconsistent");
						RequestDispatcher rd = request.getRequestDispatcher("/ajaxmessage.jsp");
						rd.forward(request, response);
						return;
					}
					LOG.debug("to value " + value);
					request.setAttribute("message", hpo_code + " - " + value);
					RequestDispatcher rd = request.getRequestDispatcher("/ajaxmessage.jsp");
					rd.forward(request, response);
				} catch(Exception e) {
					LOG.error(e.toString());
					PrintWriter wr = response.getWriter();
					wr.println("There was an error during server processing of the annotation. Please report these details: " + e.toString());
					return;
				}
				return;
			} else if (op.equals("deletePatient")){
				LOG.debug("Deleting annotation...");
				ps.reset();
				ses.removeAttribute("phenotype");
				ps = new PhenotypeState();
				ses.setAttribute("phenotype", ps);
				
				
				RequestDispatcher rd = request.getRequestDispatcher("/annotations.jsp");
				rd.forward(request, response);
				
			} else if (op.equals("getDiseases")) {
				
				LOG.debug("Getting potential diseases");
				Set<DiagnosisBean> annots = ps.getAnnotations();
				if(annots == null || annots.isEmpty()){
					LOG.error("No annotations found");
					request.setAttribute("message", "<p>No links found in database for selected phenotype</p>");
					RequestDispatcher rd = request.getRequestDispatcher("/annotations.jsp");
					rd.forward(request, response);
					return;
				}
				LOG.debug("Found " + annots.size() + " annotations");
				ArrayList<DiagnosisBean> annot_array = new ArrayList<DiagnosisBean>(annots);
				java.util.Collections.sort(annot_array);
				request.setAttribute("annotations", annot_array);
				ArrayList<IRI> code_list = new ArrayList<IRI>();
				try {
					code_list.addAll(ps.getPatientPhenotypes());
				} catch (HPOException e) {
					LOG.error(e.toString());
					PrintWriter wr = response.getWriter();
					wr.println("There was an error during server processing of the annotation. Please report these details: " + e.toString());
					return;
				}
				request.setAttribute("hpocodes", new ArrayList<String>());
		
				
				
				RequestDispatcher rd = request.getRequestDispatcher("/annotations.jsp");
				rd.forward(request, response);
				return;
			} else if (op.equals("getSuggestions")) {
				//PhenotypeOntologySequoia hp = (PhenotypeOntologySequoia) OntologyFactory.getOntology();
				String patient_id = request.getParameter("patient");
				if(patient_id == null){
					LOG.error("Request to HPOnto.getSuggestions with no patient set");
					response.sendError(400, "Request to HPOnto with no patient defined");
				}	
				List<Tickbox> hpo_suggestions;
				try {
					hpo_suggestions = ps.getSymptomSuggestions();
					if(hpo_suggestions == null) {
						LOG.error("Error in suggestions!");
						response.sendError(400, "Did not get any phenotype suggestions");
					}else {
						request.setAttribute("hplist", hpo_suggestions);
						RequestDispatcher rd = request.getRequestDispatcher("/hpocodelist.jsp");
						rd.forward(request, response);
					}
				} catch (HPOException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					LOG.error("Error in suggestions!: " + e.toString());
					response.sendError(400, "Error with getting phenotype suggestions" + e.toString());
				}
				return;
			} else if (op.equals("getSelections")) {
				PhenotypeOntologySequoia hp = (PhenotypeOntologySequoia) OntologyFactory.getOntology();
				String patient_id = request.getParameter("patient");
				if(patient_id == null){
					LOG.error("Request to HPOnto.getSuggestions with no patient set");
					response.sendError(400, "Request to HPOnto with no patient defined");
				}	
				
				try {
					Set<IRI> pos_selections = ps.getPatientPhenotypes();
					Set<IRI> neg_selections = ps.getPatientNegPhenotypes();	
					Set<IRI> selections = ps.getPatientPhenotypes();
					selections.addAll(neg_selections);
					if(selections == null || pos_selections == null || neg_selections== null)
						LOG.error("Error in suggestions!");
					else {
						LOG.error("Positive: " + pos_selections.size());
						List<Tickbox> sel_list = hp.iriToTickbox(new ArrayList<IRI>(selections), false);
						for(Tickbox t : sel_list) {
							String hpoLocalName = t.getHpoLocalName();
							String hpo_iri = HPUtils.getHPuri(hpoLocalName);
							IRI phenoiri = IRI.create(hpo_iri);	
							if(pos_selections.contains(phenoiri))
								t.setResponse(ResponseType.PRESENT);
							if(neg_selections.contains(phenoiri))
								t.setResponse(ResponseType.NOT_PRESENT);
							LOG.error(phenoiri.toString());
						}
						request.setAttribute("sel_list", sel_list);
						
					}
					
					RequestDispatcher rd = request.getRequestDispatcher("/selections.jsp");
					rd.forward(request, response);
				} catch (HPOException e) {
					LOG.error("Problem getting selections. Exception: " + e.toString());
					e.printStackTrace();
					request.setAttribute("error", "Could not get selections");
					return;
				}
			} else if (op.equals("validate")) {
				LOG.debug("Validating ...");
				PatientPhenotype p = (PatientPhenotype) request.getSession().getAttribute("patient");
				ValidityReport rep = p.validate();
				if(!rep.isValid()){
					PrintWriter wr = response.getWriter();
					wr.println("There was an error in the validation of the patient phenotype: " + rep.toString());
					return;
				}
			}
		}
	
		private boolean setPhenotype(PhenotypeState ps, String hpo_code, String value) throws OWLOntologyStorageException, HPOException, IOException{
			if(value.equals("YES")) {
				ps.removePatientNegPhenotype(hpo_code);
				//ps.removePatientNegPhenotypeReason(hpo_code);
				return ps.setPositivePatientPhenotype(hpo_code);
			} else if (value.equals("NO") || 
					value.equals(ResponseType.NOT_PRESENT.toString())
					) {
				ps.removePatientPositivePhenotype(hpo_code);
				//ps.removePatientNegPhenotypeReason(hpo_code);
				return ps.setNegativePatientPhenotype(hpo_code);
			} else if (value.equals("NA") || 
					value.equals(ResponseType.CANNOT_CHECK.toString()) ||
					value.equals(ResponseType.CANNOT_PRESENT.toString())  ||
					value.equals(ResponseType.TOO_YOUNG.toString())
					){
				ps.removePatientPositivePhenotype(hpo_code);
				ps.removePatientNegPhenotype(hpo_code);
				//ps.setNegativePhenotypeReason(hpo_code, ResponseType.valueOf(value));
				return true;
			} else
				throw new HPOException("Invalid value of parameter \"value\": " + value + ". valid values are YES, NO and NA.");
		}
	}
