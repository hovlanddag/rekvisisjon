<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page
	import="java.util.List,java.util.Map,no.uio.ifi.logid.rekvisisjon.RequisitionController,no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeList,no.uio.ifi.logid.rekvisisjon.hpo.Tickbox,no.uio.ifi.logid.rekvisisjon.hpo.DiagnosisBean"%>
<jsp:useBean id="list"
	type="no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeList" scope="request"></jsp:useBean>
<jsp:useBean id="tops"
	type="java.util.List<no.uio.ifi.logid.rekvisisjon.hpo.Tickbox>"
	scope="request"></jsp:useBean>
<jsp:useBean id="patients"
	type="java.util.List<no.uio.ifi.logid.rekvisisjon.hpo.Tickbox>"
	scope="request"></jsp:useBean>
<html>
<head>
<!-- <link rel="stylesheet" type="text/css" href="css/req.css">  -->
<script src="js/req.js"></script>
</head>
<body onLoad="updateSelected()">

<div id="navigate_hpo" style="align:left;width:60%;overflow:scroll">
	<c:forEach items="${list.getList()}" var="cat">
		<h2>
			<button type="button" id="expandCat:${cat.key}"
				onclick="expandCat('${cat.key}', this)">+</button>
			<c:out value="${cat.key}"></c:out>
		</h2>

		<div id="div:cat:${cat.key}" style="display: none">
			<c:set value="${cat.key}" var="parent" />
			<c:set value="${cat.value}" var="ticks" />
			<%@ include file="tickboxlist.jspf"%>
		</div>
		<br />
	</c:forEach>
	</div>
<div id="selected_codes_box"style="position:absolute;right:20px;top:40px;width:30%;height:200px;overflow:scroll">
<h3>Phenotype</h3>
</div>

<div id="disease_suggestion_box"
		style="position: absolute; right: 20px; top: 240px; width: 30%; height: 400px; overflow: scroll">
		<h3>
			Suggested diseases
		</h3>
		<div id="div:feedback:diseases">
			<p id="diseases">This text will change when the phenotype is
				specified</p>
		</div>
	</div>
	<div id="phenotype_suggestion_box"
		style="position: absolute; right: 20px; top: 680px; width: 30%; height: 500px; overflow: scroll">
		<h3>
			Suggested phenotypes
		</h3>
		<div id="div:feedback:suggestions">
			<p id="suggestions">This text will change when the phenotype is
				specified</p>
		</div>
	</div>
	<a href="?operation=deletePatient&patient=default">Reset</a>
</body>
</html>
