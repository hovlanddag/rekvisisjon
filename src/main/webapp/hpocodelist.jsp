
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page
	import="java.util.List,java.util.ArrayList,java.util.Map,no.uio.ifi.logid.rekvisisjon.RequisitionController,no.uio.ifi.logid.rekvisisjon.hpo.DiagnosisBean"%>

<jsp:useBean id="hplist" type="java.util.ArrayList" scope="request"></jsp:useBean>


<c:set value="${hplist}" var="hplist" />

<ul>
<c:forEach items="${hplist}" var="pheno">
	<li><c:out value="${pheno.label}" /> 
	(<c:out value="${pheno.hpoLocalName}" />) 
	</li> 
</c:forEach>
</ul>
 