# rekvisisjon
BigMed WP1 HP phenotyping requisition project

To install, put hp.owl  and phenotype_annotation.tab from HPO and rekvisisjon.csv in the folder marked as "configfolder" in src/main/webapp/WEB-INF/web.xml, by default "/var/lib/rekvisisjon". The files hp.owl and phenotype_annotation.tab can be downloaded from hpo.jax.org, and an example of rekvisisjon.csv can be found in src/test/resources


The current config uses sequoia, so you need to install sequoia.jar from https://www.cs.ox.ac.uk/isg/tools/Sequoia/ by the comandline
 mvn install:install-file -Dfile=Sequoia.jar -DgroupId=com.sequoiareasoner -DartifactId=sequoia -Dversion=1.0.0 -Dpackaging=jar|

 To use remote access with a fuseki rdf store (which is currently not enabled in the code), use the two configuration files under src/main/fuseki/configuration. Then upload hp.owl into the named graph "http://purl.obolibrary.org/obo/hp.owl" in rekvisisjon-store only.
 