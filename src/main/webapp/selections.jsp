
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page
	import="java.util.List,java.util.ArrayList,java.util.Map,no.uio.ifi.logid.rekvisisjon.RequisitionController"%>

<jsp:useBean id="sel_list" type="java.util.ArrayList" scope="request"></jsp:useBean>

<c:set value="${sel_list}" var="ticks" />
<table>
	<thead>
		<td>Yes</td>
		<td>No</td>
		<td>?</td>
	</thead>
	<c:forEach items="${ticks}" var="tick">
		<tr valign="top">
			<td><input class="phenoradio" type="radio"
				id="YES:${tick.hpoParent}/${tick.hpoLocalName}"
				name="${tick.hpoParent}/${tick.hpoLocalName}" value="YES"
				${tick.yesChecked}
				onclick="setPhenotype('${tick.hpoLocalName}', 'YES', this); hideReasonSelector('${tick.hpoLocalName}')" />
			</td>
			<td><input class="phenoradio" type="radio"
				id="NO:${tick.hpoParent}/${tick.hpoLocalName}"
				name="${tick.hpoParent}/${tick.hpoLocalName}" value="NO"
				${tick.noChecked}
				onclick="setPhenotype('${tick.hpoLocalName}', 'NO', this); showReasonSelector('${tick.hpoLocalName}')" />
			</td>
			<td><input class="phenoradio" type="radio"
				name="${tick.hpoParent}/${tick.hpoLocalName}"
				id="NA:${tick.hpoParent}/${tick.hpoLocalName}" value="N/A"
				onclick="setPhenotype('${tick.hpoLocalName}', 'NA', this); clickedNAButton('${tick.hpoLocalName}'); hideReasonSelector('${tick.hpoLocalName}')"
				/></td>
				<c:set var="reason_display" value="none" />
				<c:if test = "${tick.noChecked == 'checked'}" >
					<c:set var="reason_display" value="visible" />
				</c:if>
				<td id="selector:reason:${tick.hpoLocalName}" style="display: ${reason_display}">
				<select id="responseType:${tick.hpoParent}/${tick.hpoLocalName}"
				onchange="setPhenotype('${tick.hpoLocalName}', value, this)">
					<option>Select reason for not present</option>
					<c:set var="enumNames"
						value="<%=no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation.enumNames%>" />
					<c:forEach items="${enumNames}" var="response">
						<option value="${response.key}">${response.value}</option>
					</c:forEach>
			</select>
			</td>
			<td><c:if test="${tick.hasSubclasses}">
					<button type="button" id="expand:${tick.hpoLocalName}"
						onclick="expandClass('${tick.hpoParent}', '${tick.hpoLocalName}', this)">+</button>
				</c:if></td>

			<td id="ROW:${tick.hpoLocalName}" style="border-style:none;border-width:5px;border-color:red"><a href="${tick.infopageurl}">${tick.label}</a></td>
			<td id="content:${tick.hpoParent}/${tick.hpoLocalName}"></td>
		</tr>
	</c:forEach>
</table>
