// Called from plus buttons on top-level categories
// Leads to showing this entry level to HPO
function expandCat(category, button) {
	var catdiv = document.getElementById("div:cat:" + category);
	catdiv.style.display = "";
	button.innerHTML = "-";
	button.onclick = function() {hideCat(category, button);}
}

//Called from the minus button created by expandCat on top-level categories
//Leads to hiding this entry level to HPO
function hideCat(category, button){
	var catdiv = document.getElementById("div:cat:" + category);
	catdiv.style.display = "none";
	button.innerHTML = "+";
	button.onclick = function() {expandCat(category, button);}
}

//Get subclasses of hpo_code, passing to function enterSubclassTree to put into document
function expandClass(hpo_parent, hpo_code) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			enterSubclassTree(hpo_code, this.responseText, hpo_parent)
		}
	};
	xhttp.open("GET", "HPOnto?operation=getSubclasses&class=" + hpo_code + "&parent=" + hpo_parent, true);
	xhttp.send();
}

// Called from the ajax function over
// Takes some new html from the server and enters it (expands the phenotype tree
function enterSubclassTree(hpo_code, response, hpo_parent){
	var plusbutton = document.getElementById("expand:" + hpo_code)
	if(plusbutton != null){
		var contentCell = document.getElementById("content:" + hpo_parent + "/" + hpo_code)
		//var plusbutton = document.getElementById("expand:" + hpo_code)
		if(response.substring(3) === hpo_code.substring(3)){
			plusbutton.innerHTML = "";
			//plusbutton.visibility = "hidden";
		} else {
			contentCell.innerHTML = response;
			plusbutton.innerHTML = "-"
				plusbutton.onclick = function() { hideSubclasses(hpo_parent, hpo_code, plusbutton); };
		}
	}
}

function showReasonSelector(hpo_code){
	var selector = document.getElementById("selector:reason:" + hpo_code);
	selector.style.display="";
}

function hideReasonSelector(hpo_code){
	var selector = document.getElementById("selector:reason:" + hpo_code);
	selector.style.display="none";
}

function hideSubclasses(hpo_parent, hpo_code) {
	var plusbutton = document.getElementById("expand:" + hpo_code)
	plusbutton.innerHTML = "+"
	plusbutton.onclick = function() { expandClass(hpo_parent, hpo_code); };
	var contentCell = document.getElementById("content:" + hpo_parent + "/" + hpo_code)
	contentCell.innerHTML = "";
}

function setYesNo(responseText, hpo_code, value, yesnobutton, cellwithcontent){
	if(responseText.indexOf("inconsistent") >= 0){
		window.alert("Error entry! Cannot set " + hpo_code + " to " + value + " because it is not consistent with earlier entries.");
		yesnobutton.checked = false;
	} else {
		var cellrow = cellwithcontent.parentNode;
		cellrow.style.borderStyle = "none";
		var hpo_row = document.getElementById("ROW:" + hpo_code);
		hpo_row.style.borderStyle = "none";
	}
}

/**
 * Highlights the hpo entries in the list. 
 * The list comes from an ajax call to the method getSuggestions
 */
function markSuggestions(suggestion_list_string){
	var suggestion_list = JSON.parse(suggestion_list_string);
	for(var i in suggestion_list){
		var hpo_code = suggestion_list[i];
		var hpo_row = document.getElementById("ROW:" + hpo_code);
		if(hpo_row != null)
			hpo_row.style.borderStyle = "solid";
	}
}

/**
 * Print out 100 first entries in suggestion list
 * The list comes from an ajax call to the method getSuggestions
 */
function printSuggestions(suggestion_list_string){
	var suggestion_list = JSON.parse(suggestion_list_string);
	var html_out = "<ul>"
	for(var i in suggestion_list){
		var hpo_code = suggestion_list[i];
		html_out += "<li><a href=\"http://www.ontobee.org/ontology/HP?iri=" + hpo_code + "\">" + hpo_code + "</a></li>";
		if(i >= 100)
			break;
	}
	html_out += "</ul>";
	return html_out;
}

/**
 * Print out selected phenotype
 * The list comes from an ajax call to the method getSuggestions
 */
function printSelections(suggestion_list_string){
	var suggestion_list = JSON.parse(suggestion_list_string);
	var html_out = "<p>Phenotype: </p><ul>"
	for(var i in suggestion_list){
		var hpo_code = suggestion_list[i];
		html_out += "<li>" + hpo_code;
		html_out += "<select onchange=\"setPhenotype('${tick.hpoLocalName}', value, this)\">";
		var answer_codes = ["PRESENT", "NOT_PRESENT", "TOO_YOUNG", "CANNOT_PRESENT", "CANNOT_CHECK"];
		var answers = ["Present", "Not present", "Age of onset not reached", "Manifestation unrealizable", "Unable to examine"]
		for(var answer in answers)
			html_out += "<option value=\"" + answer_codes[answer] + "\">" + answers[answer] + "</option>";
		html_out += "</select>"+ "</li>";
	}
	html_out += "</ul>";
	return html_out;
}

/** 
 * Changes buttons about the phenotype to be Yes
 * @returns
 */
function clickedYESButton(code){
	button = document.getElementById("YES:" + code);
	button.style.visibility = "hidden";
	button.style.display = "none";
	nabutton = document.getElementById("NA:" + code);
	nabutton.style.visibility = "visible";
	nabutton.style.display = "inline";
	
}



/** 
 * Changes buttons about the phenotype to be Yes
 * @returns
 */
function clickedNAButton(code){
	button = document.getElementById("NO:" + code);
	button.style.visibility="hidden";
	button.style.visibility="none";
	yesbutton = document.getElementById("YES:" + code);
	yesbutton.style.visibility = "visible";
	yesbutton.style.visibility = "inline";
}



/**
 * Called when "YES", "NO", "?" or reason for NO is clicked on a phenotype
 * value can be "YES", "NO", "NA" or reason
 */
function setPhenotype(hpo_code, value, yesnobutton) {
	/*if(value != "YES" && value != "NO" && value != "NA"){
		window.alert("Program error. (Invalid argument \"value\": " + value + " passed to function setPhenotype");
		return;
	}*/
	patient_id = "default";
	var diseases = document.getElementById("disease_suggestion_box");
	//diseases.innerHTML = "Waiting for server response...";
	
	var cellwithbutton = yesnobutton.parentNode;
	var cellrow = cellwithbutton.parentNode;
	var cellwithcontent = cellrow.lastChild;
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			setYesNo(this.responseText, hpo_code, value, yesnobutton, cellwithcontent);
			var xhttp2 = new XMLHttpRequest();
			xhttp2.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var diseaseText = this.responseText;
					var diseases = document.getElementById("div:feedback:diseases");
					diseases.innerHTML = diseaseText;
				}
			}
			xhttp2.open("GET", "HPOnto?operation=getDiseases&patient=" + patient_id, true);
			xhttp2.send();
			var xhttp3 = new XMLHttpRequest();
			xhttp3.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var suggestion_list = this.responseText;
					//markSuggestions(suggestion_list);
					var suggestions = document.getElementById("div:feedback:suggestions");
					suggestions.innerHTML = suggestion_list;
					//var sugg_string =  printSuggestions(suggestion_list);
					//suggestions.innerHTML = sugg_string;
				}
			}
			xhttp3.open("GET", "HPOnto?operation=getSuggestions&patient=" + patient_id, true);
			xhttp3.send();
		}
	};
	xhttp.open("GET", "HPOnto?operation=setAnnotation&value=" + value + "&class=" + hpo_code + "&patient=" + patient_id, true);
	xhttp.send();
	
	// Venter litt pga. ellers hentes gammel info
	setTimeout(updateSelected, 1000);
}

/**
 * Oppdaterer valgte
 * @returns
 */
function updateSelected(){
	var xhttp_sel = new XMLHttpRequest();
	xhttp_sel.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var selected_list = this.responseText;
			var selected_box = document.getElementById("selected_codes_box");
			//var sel_string =  printSelections(selected_list);
			selected_box.innerHTML = selected_list;
			//setTimeout(updateSelected, 1000);
		}
	};
	xhttp_sel.open("GET", "HPOnto?operation=getSelections&patient=default", true);
	xhttp_sel.send();
}

