package no.uio.ifi.logid.rekvisisjon;

import java.util.List;

import javax.servlet.ServletConfig;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;

import no.uio.ifi.logid.rekvisisjon.RequisitionController;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeList;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;

import static org.junit.Assert.*;


public class RequisitionControllerTest {

	public static String update_endpoint = "http://localhost:9090/fuseki/rekvisisjon-store/update";
	public static String query_endpoint = "http://localhost:9090/fuseki/rekvisisjon-reason/query";
	public static String data_endpoint = "http://localhost:9090/fuseki/rekvisisjon-store/data";
	
	
	@Test
	public void test() throws Exception {
		
		MockServletContext ctxt = new MockServletContext();
		ctxt.addInitParameter("configfolder", "src/test/resources");
		ctxt.addInitParameter("queryendpoint", query_endpoint);
		ctxt.addInitParameter("updateendpoint", update_endpoint);
		ctxt.addInitParameter("graphendpoint", data_endpoint);
		MockHttpServletRequest request = new MockHttpServletRequest(ctxt);
		
		MockHttpServletResponse response = new MockHttpServletResponse();
		RequisitionController ctrl = new RequisitionController();
		
		ctrl.init();
		new RequisitionController().doPost(request, response);
		
		List<Tickbox> patients = (List<Tickbox>) request.getAttribute("patients");
		assertTrue(patients != null);
		
		List<Tickbox> tops = (List<Tickbox>) request.getAttribute("tops");
		assertTrue(tops != null);
		
		PhenotypeList list = (PhenotypeList) request.getAttribute("list");
		assertTrue(list != null);
		
	}

}
