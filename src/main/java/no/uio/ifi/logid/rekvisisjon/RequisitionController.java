package no.uio.ifi.logid.rekvisisjon;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.jena.atlas.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Session;

import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntology;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotationsFileReader;
import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntologyFile;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeState;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;

public class RequisitionController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2568875915494996139L;

	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(RequisitionController.class);

	public RequisitionController(){
		super();
	}

	/**
	 * Sets up the data structures that read from file
	 * @throws HPOException 
	 * @throws IOException 
	 * @throws InterruptedException 
	 *
	 */
	public static void setupDataStructures(ServletContext ctxt) throws IOException, InterruptedException {
		String config_file_path = ctxt.getInitParameter("configfolder");
		ConfigFiles cfg = ConfigFiles.getInstance(config_file_path);
		if(!cfg.hasHPOwl())
			cfg.downloadHPOwl();
		OntologyFactory.init(config_file_path, OntologyFactory.backend_name.Sequoia);
		//PhenotypeOntologyFile.getInstance(config_file_path);
		PhenotypeListReader.getInstance(config_file_path);
		PatientPhenotypeFileReader.getInstance(config_file_path);
		HPAnnotationsFileReader.getInstance(config_file_path);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.doGetOrPost(request, response);
		} catch (InterruptedException e) {
			// This is if the lock on the ontology intialization is interrupted
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	public void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			this.doGetOrPost(request, response);
		} catch (InterruptedException e) {
			// This is if the lock on the ontology intialization is interrupted
			e.printStackTrace();
			throw new ServletException(e);
		}
	}


	private void doGetOrPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException, InterruptedException {

		setupDataStructures(request.getServletContext());
		//String op = (String) request.getParameter("operation");
		PhenotypeOntology hpowl = OntologyFactory.getOntology();
		HttpSession session = request.getSession(true);
		
		PhenotypeState ps = (PhenotypeState) session.getAttribute("phenotype");
		if(ps == null){
			ps = new PhenotypeState();
			session.setAttribute("phenotype", ps);
		}
		PhenotypeListReader list;
		RequisitionController.setupDataStructures(request.getServletContext());
		RequestDispatcher rd = request.getRequestDispatcher("/req.jsp");

		list = PhenotypeListReader.getInstance();
		request.setAttribute("list", list.getTickboxes());

		List<Tickbox> patients = hpowl.getPatientIDList();
		request.setAttribute("patients", patients);
		
		try {
			List<Tickbox> tops = hpowl.getTopClassTickboxes(true);
			request.setAttribute("tops", tops);
		} catch (HPOException e) {
			LOG.error("Error getting list of top level HPO Classes: " + e.toString());
			throw new ServletException("Error getting list of top level HPO Classes: ", e);
		}
		rd.forward(request, response);

	}


}
