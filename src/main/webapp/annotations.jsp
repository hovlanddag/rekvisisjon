
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page
	import="java.util.List,java.util.ArrayList,java.util.Map,no.uio.ifi.logid.rekvisisjon.RequisitionController,no.uio.ifi.logid.rekvisisjon.hpo.DiagnosisBean"%>

<jsp:useBean id="annotations" type="java.util.ArrayList" scope="request"></jsp:useBean>


<ul>
<c:forEach items="${annotations}" var="annot">
	<li><c:if test="${annot.qualifier} != null">
	<c:out value="${annot.qualifier}" /> 
	</c:if><c:out value="${annot.db_name}" /> 
	(<c:out value="${annot.db_reference}" /> - <a target="_blank" href=<c:out value="${annot.web_link}" />><c:out value="${annot.web_link}" /></a>, 
	from <c:out value="${annot.hpo_id}" />) 
	<!-- Frequency: <c:out value="${annot.disease_frequency}" /> -->
	<!-- Other symptoms: <c:out value="${annot.other_hpo_ids}" /> -->
	Lacking symptoms: <c:out value="${annot.lacking_hpos}" /></li> 
</c:forEach>
</ul>
